import { ChevronDownIcon } from '@chakra-ui/icons';
import { Box, Button, FormControl, Input, Select, Text, SimpleGrid } from '@chakra-ui/react';
import { Controller, useFormContext } from 'react-hook-form';

const WarningText = ({ message }: { message: string | undefined }) => (
  <Text fontSize="xs" color="red" mt={2}>
    {message || ''}
  </Text>
);

const CreditCardForm = () => {
  const marginBottom = 10;

  const {
    control,
    formState: { isValid },
  } = useFormContext();

  const formatCreditCardNumber = (node: string) => {
    const v = node?.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
    const matches = v.match(/\d{4,16}/g);
    const match = (matches && matches[0]) || '';

    const parts = [];
    for (let i = 0, len = match.length; i < len; i += 4) {
      parts.push(match.substring(i, i + 4));
    }

    return parts.length ? parts.join(' ') : node;
  };

  return (
    <>
      <Controller
        name="creditCardNumber"
        control={control}
        render={({ field: { onChange, ...field }, fieldState }) => (
          <FormControl isRequired mt={14} mb={marginBottom}>
            <Input
              placeholder="Número do Cartão*"
              size="lg"
              onChange={(e) => onChange(formatCreditCardNumber(e?.target?.value))}
              variant="flushed"
              maxLength={19}
              isInvalid={fieldState.invalid}
              {...field}
            />
            <WarningText message={fieldState?.error?.message} />
          </FormControl>
        )}
      />

      <Controller
        name="name"
        control={control}
        render={({ field, fieldState }) => (
          <FormControl isRequired mb={marginBottom}>
            <Input
              size="lg"
              variant="flushed"
              placeholder="Nome (igual o do cartão)*"
              maxLength={18}
              textTransform="uppercase"
              isInvalid={fieldState.invalid}
              {...field}
            />
            <WarningText message={fieldState?.error?.message} />
          </FormControl>
        )}
      />

      <SimpleGrid columns={2} spacing={10} mb={marginBottom}>
        <Box>
          <Controller
            name="validDate"
            control={control}
            render={({ field, fieldState }) => (
              <FormControl isRequired>
                <Input
                  size="lg"
                  variant="flushed"
                  maxLength={5}
                  placeholder="Validade* (mes/ano)"
                  isInvalid={fieldState.invalid}
                  {...field}
                />
                <WarningText message={fieldState?.error?.message} />
              </FormControl>
            )}
          />
        </Box>

        <Box>
          <Controller
            name="cvv"
            control={control}
            render={({ field, fieldState }) => (
              <FormControl isRequired>
                <Input
                  size="lg"
                  variant="flushed"
                  placeholder="CVV*"
                  minLength={3}
                  maxLength={3}
                  isInvalid={fieldState.invalid}
                  {...field}
                />
                <WarningText message={fieldState?.error?.message} />
              </FormControl>
            )}
          />
        </Box>
      </SimpleGrid>

      <Controller
        name="numberOfInstallments"
        control={control}
        render={({ field, fieldState }) => (
          <FormControl isRequired mb={marginBottom}>
            <Select
              size="lg"
              variant="flushed"
              icon={<ChevronDownIcon />}
              iconColor="brand.100"
              placeholder="Número de parcelas*"
              isInvalid={fieldState.invalid}
              {...field}
            >
              {[...Array.from({ length: 10 }, (_, i) => i)].map((value, key) => (
                <option value={value + 1} key={key}>
                  {value + 1}
                </option>
              ))}
            </Select>
          </FormControl>
        )}
      />

      <Box textAlign={['center', 'right']} mt={marginBottom}>
        <Button disabled={!isValid} colorScheme="brand" size="lg">
          Continuar
        </Button>
      </Box>
    </>
  );
};

export default CreditCardForm;
