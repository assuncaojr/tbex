import * as yup from 'yup';

export type CredidCardType = {
  creditCardNumber: string;
  name: string;
  validDate: string;
  cvv: string;
  numberOfInstallments?: string;
};

const messages = {
  creditCardNumber: 'Número de cartão inválido',
  name: 'Insira seu nome completo',
  validDate: 'Data inválida',
  cvv: 'Código inválido',
  numberOfInstallments: 'Insira o número de parcelas',
};

export const CREDIT_CARD_INITIAL_STATE: CredidCardType = {
  creditCardNumber: '',
  name: '',
  validDate: '',
  cvv: '',
  numberOfInstallments: '',
};

export default yup.object({
  creditCardNumber: yup.string().min(19, messages.creditCardNumber).required(messages.creditCardNumber),
  name: yup.string().min(8, messages.name).required(messages.name),
  validDate: yup
    .string()
    .matches(/((0[1-9]|1[0-2])[/][12]\d{1})/, messages.validDate)
    .required(messages.validDate),
  cvv: yup
    .string()
    .min(3, messages.cvv)
    .matches(/[0-9]{3}$/, messages.cvv)
    .required(messages.cvv),
  numberOfInstallments: yup.string().required(messages.numberOfInstallments),
});
