import { CheckCircleIcon, ChevronRightIcon } from '@chakra-ui/icons';
import { Box, Breadcrumb as ChkBreadcrumb, BreadcrumbItem, BreadcrumbLink } from '@chakra-ui/react';

const CircleIcon = ({ step, ...props }: { step: string }) => (
  <Box
    {...props}
    boxSize={6}
    mr={2}
    borderColor="brand.100"
    borderRadius="full"
    borderWidth={2}
    textAlign="center"
    padding="1px 0 0 0"
    fontSize={12}
    fontWeight="bold"
  >
    {step}
  </Box>
);

export const Breadcrumb = () => {
  const breadItems = [
    {
      text: 'Carrinho',
      icon: <CheckCircleIcon mr={2} boxSize={6} />,
    },
    {
      text: 'Pagamento',
      icon: <CircleIcon step="2" />,
    },
    {
      text: 'Confirmação',
      icon: <CircleIcon step="3" />,
    },
  ];

  return (
    <ChkBreadcrumb
      mb={6}
      display={{ base: 'none', sm: 'block' }}
      spacing={[0, 0, 0, 6]}
      separator={<ChevronRightIcon boxSize={8} color="brand.100" />}
    >
      {breadItems.map(({ text, icon }, key) => (
        <BreadcrumbItem key={key}>
          <BreadcrumbLink display="flex" alignItems="center" href="#" fontSize={[10, 10, 12, 16]}>
            {icon}
            <span>{text}</span>
          </BreadcrumbLink>
        </BreadcrumbItem>
      ))}
    </ChkBreadcrumb>
  );
};
