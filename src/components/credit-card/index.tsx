import { chakra, Box, Text, SimpleGrid, Image, useToken } from '@chakra-ui/react';
import BgGray from '../../assets/img/card-gray.svg';
import BgCard from '../../assets/img/card-blue.svg';
import LogoVisa from '../../assets/img/visa.svg';
import { CredidCardType } from '../form/utils';

const CreditCard = ({ creditCardNumber, name, validDate }: CredidCardType) => {
  const [borderRadius] = useToken('radii', ['md']);
  const hasStartedTypingCard = creditCardNumber?.length > 4;

  const Card = chakra(Box, {
    baseStyle: {
      backgroundImage: hasStartedTypingCard ? BgCard : BgGray,
      backgroundSize: 'cover',
      boxShadow: '0px 16px 15px -11px rgba(0, 0, 0, .7)',
      color: 'white',
      borderRadius,
      position: 'relative',
      textShadow: 'rgba(0, 0, 0, .5) 0px 1px 0',
    },
  });

  const fontSizes = { base: '2xl', md: '1xl', lg: '3xl' };
  const fsSecondaryText = { base: 'md', md: 'md', lg: 'xl' };

  return (
    <Card
      w={{
        base: '100%',
        sm: 371,
        md: 371,
        lg: 456,
        xl: 500,
      }}
      h={{
        base: 200,
        sm: 222,
        md: 222,
        lg: 273,
        xl: 300,
      }}
      padding={[6]}
    >
      {hasStartedTypingCard && <Image src={LogoVisa} alt="" />}

      {!hasStartedTypingCard && <Box w={133} h={35} />}

      <Text
        fontSize={fontSizes}
        mb={4}
        mt={{
          base: 10,
          md: 12,
          lg: 20,
          xl: 24,
        }}
      >
        {creditCardNumber || '**** **** **** ****'}
      </Text>

      <SimpleGrid columns={2}>
        <Box w={200}>
          <Text fontSize={fsSecondaryText} textTransform="uppercase">
            {name || 'Nome do Titular'}
          </Text>
        </Box>

        <Box>
          <Text align="right" fontSize={fsSecondaryText}>
            {validDate || '00/00'}
          </Text>
        </Box>
      </SimpleGrid>
    </Card>
  );
};

export default CreditCard;
