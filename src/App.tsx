import { ChakraProvider, Text, Flex, Box } from '@chakra-ui/react';
import { ChevronLeftIcon } from '@chakra-ui/icons';
import CreditCard from './components/credit-card';
import CreditCardForm from './components/form';
import theme from './theme';
import { Breadcrumb } from './components/breacrumb';
import IconCard from './assets/img/ico-card.svg';
import { FormProvider, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import schema, { CredidCardType, CREDIT_CARD_INITIAL_STATE } from './components/form/utils';

export const App = () => {
  const methods = useForm<CredidCardType>({
    mode: 'onChange',
    resolver: yupResolver(schema),
    defaultValues: CREDIT_CARD_INITIAL_STATE,
  });

  const [creditCardNumber, name, validDate, cvv] = methods.watch(['creditCardNumber', 'name', 'validDate', 'cvv']);

  const formData: CredidCardType = {
    creditCardNumber,
    name,
    validDate,
    cvv,
  };

  return (
    <ChakraProvider theme={theme}>
      <Flex display={['block', 'block', 'block', 'flex']}>
        <Box
          width={['100vw', '100vw', '100vw', '35vw', '30vw']}
          height={{
            base: '300px',
            sm: '100vh',
          }}
          bg="brand.100"
          padding={10}
        >
          <Flex color="white" alignItems="center" mb={[6, 14]}>
            <ChevronLeftIcon />
            <Text display={{ base: 'none', sm: 'block' }}>Alterar forma de pagamento</Text>

            <Text w="full" fontWeight="bold" display={{ base: 'inline-block', sm: 'none' }} textAlign="center">
              Etapa 2 - 3
            </Text>
          </Flex>

          <Flex color="white" alignItems="center" mb={10}>
            <img src={IconCard} alt="" />
            <Text fontSize="2xl" ml={4}>
              Adicione um novo cartão de crédito
            </Text>
          </Flex>

          <Box display={[null, null, 'flex', 'block']} justifyContent="center">
            <CreditCard {...formData} />
          </Box>
        </Box>

        <Box width={[null, null, 'full', '62vw', '70vw']} h="100vh" mt={[16, 0]} padding={[10, 24]}>
          <Breadcrumb />

          <FormProvider {...methods}>
            <CreditCardForm />
          </FormProvider>
        </Box>
      </Flex>
    </ChakraProvider>
  );
};
