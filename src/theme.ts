import { extendTheme } from '@chakra-ui/react';

const colors = {
  primary: '#4BDE95',
};

const theme = extendTheme({
  colors: {
    brand: {
      50: '#dffef3',
      100: colors.primary,
      200: '#90ecc5',
      300: '#66e3aa',
      400: '#3ddb8d',
      500: '#24c27e',
      600: '#18976a',
      700: '#0c6c52',
      800: '#014235',
      900: '#001810',
    },
  },
  styles: {
    global: {
      'ol.chakra-breadcrumb__list': {
        display: 'flex',
        justifyContent: 'right',
        '& a': {
          color: 'brand.100',
        },
      },
      // Color Schemes for Input are not implemented in the default theme
      '.chakra-input:focus, .chakra-select:focus': {
        boxShadow: `0px 1px 0px 0px ${colors.primary} !important`,
      },
      '.chakra-select:first-child': {
        color: 'gray.400',
      },
    },
  },
});

export default theme;
